<?php

namespace common\models;

use Yii;
use common\db\ActiveRecord;
use common\models\User;
use common\models\project\Task;
use common\models\project\TaskList;
use common\models\project\Job;
use common\models\project\JobList;
use common\models\project\File;
use common\models\project\Message;
use common\models\project\Member;
use common\models\project\Activity;
use common\models\project\Comment;

/**
 * This is the model class for table "project".
 *
 * @property string $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property integer $is_active
 * @property integer $is_archived
 * @property string $created_at
 * @property string $updated_at
 * @property string $creator_id
 * @property string $updater_id
 * @property string $status
 * @property string $start_date
 * @property string $closed_at
 *
 * @property User $user
 * @property Activity[] $activities
 * @property Comment[] $comments
 * @property File[] $files
 * @property Job[] $jobs
 * @property JobList[] $jobLists
 * @property Member[] $members
 * @property Message[] $messages
 * @property Task[] $tasks
 * @property TaskList[] $taskLists
 */
class Project extends ActiveRecord
{
    public static $statusOptions = [ 'new' => 'New', 'open' => 'Open', 'on_hold' => 'On hold', 'closed' => 'Closed' ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'updater_id', 'title', 'status'], 'required'],
            ['status', 'in', 'range'=>['new', 'open', 'on_hold', 'closed']],
            ['status', 'default', 'value'=>'new'],
            [['is_active', 'is_archived'], 'in', 'range'=>[1, 0]],
            ['is_active', 'default', 'value'=>1],
            ['is_archived', 'default', 'value'=>0],
            ['start_date', 'default', 'value'=>date('Y-m-d')],
            [['creator_id', 'is_active', 'is_archived', 'updater_id'], 'integer'],
            [['description', 'status'], 'string'],
            [['title', 'short_description'], 'string', 'max' => 255],
            ['status', 'in', 'range'=>array_keys(self::$statusOptions)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_id' => 'Creator Id',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'is_archived' => 'Is Archived',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updater_id' => 'Updater Id',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'closed_at' => 'Closed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobLists()
    {
        return $this->hasMany(JobList::className(), ['project_id' => 'id'])
         ->where('task_id Is Null');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['project_id' => 'id'])
         ->where('task_id Is Null')
         ->joinWith('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskLists()
    {
        return $this->hasMany(TaskList::className(), ['project_id' => 'id']);
    }
    
    public function getTaskListOptions() {
     $ret = [];
     $items = $this->getTaskLists()->all();
     foreach ($items as $item) {
      $ret[$item->id] = $item->title;
     }
     return $ret;
    }
    
    public function getJobListOptions() {
     $ret = [];
     $items = $this->getJobLists()->all();
     foreach ($items as $item) {
      $ret[$item->id] = $item->title;
     }
     return $ret;
    }
    
    public function getMemberOptions() {
     $ret = [];
     $items = $this->getMembers()->with('user')->all();
     foreach ($items as $item) {
      $user = $item->getUser()->one();
      $ret[$user->id] = $user->display_name;
     }
     return $ret;
    }
}
