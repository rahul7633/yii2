<?php

namespace common\models\project;

use Yii;

/**
 * This is the model class for table "project_task_list".
 *
 * @property string $id
 * @property string $project_id
 * @property string $creator_id
 * @property string $title
 * @property string $short_description
 * @property integer $is_archived
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $updater_id
 *
 * @property ProjectTask[] $projectTasks
 * @property User $user
 * @property Project $project
 */
class TaskList extends \common\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_task_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'creator_id', 'updater_id', 'title', 'short_description'], 'required'],
            [['project_id', 'creator_id', 'is_archived', 'is_deleted', 'updater_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'short_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'creator_id' => 'Creator ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'is_archived' => 'Is Archived',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updater_id' => 'Updater ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectTasks()
    {
        return $this->hasMany(ProjectTask::className(), ['list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
