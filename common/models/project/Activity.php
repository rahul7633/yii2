<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\Task;
use common\models\project\Comment;

/**
 * This is the model class for table "project_activity".
 *
 * @property string $id
 * @property string $project_id
 * @property string $task_id
 * @property string $creator_id
 * @property string $updater_id
 * @property string $item_id
 * @property string $item_type
 * @property string $data
 * @property integer $is_active
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Project $project
 * @property Task $task
 * @property Comment[] $Comments
 */
class Activity extends \common\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_activity}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'creator_id', 'updater_id', 'data'], 'required'],
            [['project_id', 'task_id', 'creator_id', 'updater_id', 'item_id', 'is_active', 'is_deleted'], 'integer'],
            [['data'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_type'], 'string', 'max' => 100],
            ['is_active', 'default', 'value'=>1],
            ['is_deleted', 'default', 'value'=>0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'task_id' => 'Task ID',
            'creator_id' => 'Creator ID',
            'updater_id' => 'Updater ID',
            'item_id' => 'Item ID',
            'item_type' => 'Item Type',
            'data' => 'Data',
            'is_active' => 'Is Active',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['activity_id' => 'id']);
    }
}
