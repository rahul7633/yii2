<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\Activity;

/**
 * This is the model class for table "project_comment".
 *
 * @property string $id
 * @property string $project_id
 * @property string $task_id
 * @property string $activity_id
 * @property string $creator_id
 * @property string $updater_id
 * @property string $body
 * @property integer $is_active
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Project $project
 * @property Task $task
 * @property Activity $activity
 */
class Comment extends \common\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'activity_id', 'creator_id', 'updater_id', 'body'], 'required'],
            [['project_id', 'task_id', 'activity_id', 'creator_id', 'updater_id', 'is_active', 'is_deleted', 'created_at', 'updated_at'], 'integer'],
            [['body'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'task_id' => 'Task ID',
            'activity_id' => 'Activity ID',
            'creator_id' => 'Creator ID',
            'updater_id' => 'Updater ID',
            'body' => 'Body',
            'is_active' => 'Is Active',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }
}
