<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\Task;
use common\models\project\Job;

/**
 * This is the model class for table "project_job_list".
 *
 * @property string $id
 * @property string $project_id
 * @property string $task_id
 * @property string $creator_id
 * @property string $title
 * @property string $short_description
 * @property integer $is_archived
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $updater_id
 *
 * @property Job[] $Jobs
 * @property User $user
 * @property Project $project
 * @property Task $task
 */
class JobList extends \common\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_job_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'creator_id', 'updater_id', 'title'], 'required'],
            [['project_id', 'task_id', 'creator_id', 'is_archived', 'is_deleted', 'updater_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'short_description'], 'string', 'max' => 255],
            [['is_archived', 'is_deleted'], 'default', 'value'=>0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'task_id' => 'Task ID',
            'creator_id' => 'Creator ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'is_archived' => 'Is Archived',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updater_id' => 'Updater ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['list_id' => 'id'])
         ->joinWith('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
}
