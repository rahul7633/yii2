<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\TaskList;
use common\models\project\JobList;
use common\models\project\Job;
use common\models\project\Message;
use common\models\project\File;
use common\models\project\Activity;
use common\models\project\Comment;

/**
 * This is the model class for table "project_task".
 *
 * @property string $id
 * @property string $parent_id
 * @property string $list_id
 * @property string $project_id
 * @property string $creator_id
 * @property string $owner_id
 * @property string $type
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property integer $is_active
 * @property integer $is_archived
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $updater_id
 * @property string $status
 * @property string $start_date
 * @property string $due_date
 * @property string $closed_at
 *
 * @property Activity[] $activities
 * @property Comment[] $comments
 * @property File[] $files
 * @property Job[] $jobs
 * @property JobList[] $jobLists
 * @property Message[] $messages
 * @property User $user
 * @property User $owner
 * @property Project $project
 * @property Task $parent
 * @property Task[] $subTasks
 * @property TaskList $taskList
 */
class Task extends \common\db\ActiveRecord
{
    
    public static $statusOptions = [
      'new' => 'New',
      'started' => 'Started',
      'on_hold' => 'On Hold',
      'ready_for_testing' => 'Ready for Testing',
      'completed' => 'Completed',
      'accepted' => 'Accepted',
      'rejected' => 'Rejected',
      'restarted' => 'Restarted',
      'closed' => 'Closed'
     ];
    
    public static $typeOptions = [
     'release' => 'Release',
     'feature' => 'Feature',
     'bug' => 'Bug',
     'chore' => 'Chore',
    ];
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'list_id', 'project_id', 'creator_id', 'owner_id', 'is_active', 'is_archived', 'is_deleted', 'updater_id'], 'integer'],
            [['project_id', 'list_id', 'creator_id', 'updater_id', 'owner_id', 'type', 'description', 'start_date', 'due_date', 'title', 'short_description', 'status'], 'required'],
            [['type', 'description', 'status'], 'string'],
            [['start_date', 'due_date'], 'safe'],
            [['title', 'short_description'], 'string', 'max' => 255],
            [['start_date', 'due_date'], 'date', 'format'=>'yyyy-mm-dd'],
            ['status', 'in', 'range' => array_keys(self::$statusOptions)],
            ['type', 'in', 'range' => array_keys(self::$typeOptions)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'list_id' => 'List ID',
            'project_id' => 'Project ID',
            'creator_id' => 'Creator ID',
            'owner_id' => 'Owner ID',
            'type' => 'Type',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'is_archived' => 'Is Archived',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updater_id' => 'Updater ID',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'due_date' => 'Due Date',
            'closed_at' => 'Closed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobLists()
    {
        return $this->hasMany(JobList::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Task::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubTasks()
    {
        return $this->hasMany(Task::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskList()
    {
        return $this->hasOne(TaskList::className(), ['id' => 'list_id']);
    }
}
