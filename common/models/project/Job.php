<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\Task;
use common\models\project\JobList;

/**
 * This is the model class for table "project_job".
 *
 * @property string $id
 * @property string $project_id
 * @property string $task_id
 * @property string $list_id
 * @property string $creator_id
 * @property string $updater_id
 * @property string $title
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $closed_at
 *
 * @property User $user
 * @property Project $project
 * @property Task $task
 * @property JobList $list
 */
class Job extends \common\db\ActiveRecord
{
    public static $statusOptions = [
    'open' => 'Open',
    'closed' => 'Closed'
      ];
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_job}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'creator_id', 'list_id', 'updater_id', 'title', 'status'], 'required'],
            [['project_id', 'task_id', 'list_id', 'creator_id', 'updater_id'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at', 'closed_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            ['status', 'in', 'range' => array_keys(self::$statusOptions)],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'task_id' => 'Task ID',
            'list_id' => 'List ID',
            'creator_id' => 'Creator ID',
            'title' => 'Title',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updater_id' => 'Updater ID',
            'closed_at' => 'Closed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(JobList::className(), ['id' => 'list_id']);
    }
}
