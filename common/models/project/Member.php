<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;

/**
 * This is the model class for table "project_member".
 *
 * @property string $id
 * @property string $project_id
 * @property string $user_id
 * @property string $role
 * @property string $creator_id
 * @property string $updater_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property User $creator
 * @property Project $project
 */
class Member extends \common\db\ActiveRecord
{
    public $user;
    public static $roleOptions = ['owner'=>'Owner', 'manager'=>'Manager', 'contributor'=>'Contributor', 'member'=>'Member', 'subscriber'=>'Subscriber'];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'user', 'user_id', 'creator_id', 'updater_id', 'role'], 'required'],
            [['project_id', 'user_id', 'creator_id', 'updater_id'], 'integer'],
            [['user', 'role'], 'string', 'max'=>255],
            [['user', 'user_id', 'project_id', 'creator_id'], 'safe'],
            ['role', 'in', 'range'=>array_keys(self::$roleOptions)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'user_id' => 'User ID',
            'role' => 'Role',
            'creator_id' => 'Creator ID',
            'updater_id' => 'Updater ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
