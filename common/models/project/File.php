<?php

namespace common\models\project;

use Yii;
use common\models\User;
use common\models\Project;
use common\models\project\Task;

/**
 * This is the model class for table "project_file".
 *
 * @property string $id
 * @property string $project_id
 * @property string $task_id
 * @property string $creator_id
 * @property string $updater_id
 * @property string $file_name
 * @property string $file_path
 * @property string $file_size
 * @property string $file_mime
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Project $project
 * @property ProjectTask $task
 */
class File extends \common\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'creator_id', 'updater_id', 'file_name', 'file_path', 'file_size', 'file_mime'], 'required'],
            [['project_id', 'task_id', 'creator_id', 'updater_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_path'], 'string', 'max' => 255],
            [['file_size', 'file_mime'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'task_id' => 'Task ID',
            'creator_id' => 'Creator ID',
            'updater_id' => 'Updater ID',
            'file_name' => 'File Name',
            'file_path' => 'File Path',
            'file_size' => 'File Size',
            'file_mime' => 'File Mime',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
}
