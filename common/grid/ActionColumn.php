<?php
namespace common\grid;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn as BaseActionColumn;

class ActionColumn extends BaseActionColumn {
 public function init() 
 {
  parent::init();
 }
 
 public function initDefaultButtons() 
 {
  if (!isset($this->buttons['view'])) {
   $this->buttons['view'] = function ($url, $model) {
    return Html::a('View', $url, [
      'title' => Yii::t('yii', 'View'),
      'data-pjax' => '0',
      'class' => 'btn btn-primary btn-xs',
      ]);
   };
  }
  
  if (!isset($this->buttons['update'])) {
   $this->buttons['update'] = function ($url, $model) {
    return Html::a('Update', $url, [
      'title' => Yii::t('yii', 'Update'),
      'data-pjax' => '0',
      'class' => 'btn btn-primary btn-xs',
      ]);
   };
  }
  
  if (!isset($this->buttons['delete'])) {
   $this->buttons['delete'] = function ($url, $model) {
    return Html::a('Delete', $url, [
      'title' => Yii::t('yii', 'Delete'),
      'data-pjax' => '0',
      'class' => 'btn btn-danger btn-xs',
      'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
      'data-method' => 'post',
      ]);
   };
  }
 }
 
}