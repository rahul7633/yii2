/* jquery document ready event */
jQuery(function($){
    $('div.grid-view a.ajax:not(.form)').live('click', function(){
    	if($(this).hasClass('disabled')) return false;
        if($(this).hasClass('del-alert') && !confirm('Are you sure you want to continue doing this operation?')) return false;

        var list_id = $(this).closest('div.grid-view').attr('id');
        $(this).addClass('disabled');

        biz.ajax({
        	type: 'post',
            url: $(this).attr('href'),
            dataType: 'json',
            success: function(data) {
                if( data==null ) return;
                if( typeof data.alert != 'undefined' ) {
                	biz.alert(data.alert);
                }
                if( typeof data.success == 'undefined' ) {
                	return;
                }
            	if( Boolean(data.success) == true ) {
            		/*$('#'+).yiiGridView('update');*/
            		$.fn.yiiGridView.update(list_id);
                }
            },
            async: false
        });
        
        return false;
    })

    $('div.list-view a.ajax:not(.form)').live('click', function(){
        if($(this).hasClass('disabled')) return false;
        if($(this).hasClass('del-alert') && !confirm('Are you sure you want to continue doing this operation?')) return false;

        var elm=this;
        var list_id=$(this).closest('div.list-view').attr('id');
        $(this).addClass('disabled');

        /*$.fn.yiiListView.update(list_id);return false;*/
        $.fn.yiiListView.update(list_id, {
            type: 'post',
            url: $(this).attr('href'),
            dataType: 'json',
            complete: function() {
                $('#'+list_id).removeClass('list-view-loading');
                $(elm).removeClass('disabled');
            },
            success: function(data) {
                if(data==null) return;
                if(typeof data.alert != 'undefined') {
                	biz.alert(data.alert);
                }
                if( typeof data.success == 'undefined' ) {
                	return;
                }
                if( Boolean(data.success) == true ) {
                	$.fn.yiiListView.update(list_id);
                }
            },
            async: false
        });
        
        return false;
    })
    
    $('div.list-view form.ajax').live('submit', function() {
        if($(this).hasClass('disabled')) return false;
        
        var elm=this;
        var list_id=$(this).closest('div.list-view').attr('id');
        $('button[type="submit"]', this).addClass('disabled');
        $(this).addClass('disabled');
        
        $.fn.yiiListView.update(list_id, {
            type: 'post',
            url: $(this).attr('action'),
            dataType: 'json',
            data: $(this).serialize(),
            complete: function() {
                $('#'+list_id).removeClass('list-view-loading');
                $('button[type="submit"]', elm).removeClass('disabled');
                $(elm).removeClass('disabled');
            },
            success: function(data) {
                if(data==null) return;
                if(typeof data.alert != 'undefined') biz.alert(data.alert);
                if(typeof data.success != 'undefined' && Boolean(data.success)===true) $.fn.yiiListView.update(list_id);
            },
            async: false
        });
        
        return false;
    })
    
    $('a.ajax.form').live('click', function() {
        var $href = $(this).data('ajax-href');
        if ($href.length>0!==true) return true;
        biz.ajax({
        url: $href,
        dataType: 'html',
        async: false,
        success: function(res) {
            $('div.dynamic-form').html(res);
            $('div.dynamic-form').prepend('<p class="text-right"><a style="color:#428bca;text-decoration:underline;" href="javascript:;" onclick="jQuery(\'div.dynamic-form\').html(\'\');">Close&nbsp;[x]</a><p>');
            $('html, body').animate({ scrollTop: $('div.dynamic-form').offset().top }, 'slow');
            documentReady();
        }
        });
        return false;
    });
    
    $("a[rel=\'form\']").live("click", function(){var frm_id="frm_"+$(this).attr("id");if($("#"+frm_id).hasClass("hide"))$("#"+frm_id).removeClass("hide");else $("#"+frm_id).addClass("hide");})
    documentReady();
});

function documentReady() {
    $('div.list-view div.items > div:even').addClass('even');
    $('div.list-view div.items > div:odd').addClass('odd');
}

function bizAjaxForm(frm_id) {
    var $frm = $('#'+frm_id);
    $frm.on('submit', function(){
        biz.ajax({
            url: $(this).attr('action'),
            beforeSend: function(){
                biz.beforeSend();
                $('input[type="submit"]', $frm).prop('disabled', true);
            },
            success: function(res) {
                $('.has-error', $frm).removeClass('has-error');

                if (typeof res.success=='undefined' || Boolean(res.success)!==true) {
                    $('input[type="submit"]', $frm).prop('disabled', false);
                }
                
                biz.success(res);
            },
            data: $(this).serialize()
        });
        return false;
    });
}

function bookmarksite(title,url) {
	if (!url) {url = window.location}
	if (!title) {title = document.title}
	var browser=navigator.userAgent.toLowerCase();
	if (window.sidebar) { // Mozilla, Firefox, Netscape
		window.sidebar.addPanel(title, url,"");
	} else if( window.external) { // IE or chrome
		if (browser.indexOf('chrome')==-1){ // ie
			window.external.AddFavorite( url, title); 
		} else { // chrome
			alert('Please Press CTRL+D (or Command+D for macs) to bookmark this page');
		}
	}
	else if(window.opera && window.print) { // Opera - automatically adds to sidebar if rel=sidebar in the tag
		return true;
	}
	else if (browser.indexOf('konqueror')!=-1) { // Konqueror
		alert('Please press CTRL+B to bookmark this page.');
	}
	else if (browser.indexOf('webkit')!=-1){ // safari
		alert('Please press CTRL+B (or Command+D for macs) to bookmark this page.');
	} else {
		alert('Your browser cannot add bookmarks using this link. Please add this link manually.')
	}
};

function postToUrl(path, params, method) {
    method = method || "post"; // Set method to post by default, if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
};

function isJSON(data) {
    var isJson = false
    try {
       // this works with JSON string and JSON object, not sure about others
       var json = $.parseJSON(data);
       isJson = typeof json === 'object' ;
    } catch (ex) {
        console.error('data is not JSON');
    }
    return isJson;
}
