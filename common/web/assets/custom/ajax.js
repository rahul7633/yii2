var bizJS = function () {
    this.baseUrl = '/yii/';
    this.XHR = {};
    
    this.ajax = function (params) {
        if ( typeof params.url == 'undefined' ) {
            return false;
        }
        var defaults = {
            type: 'post',
            dataType: 'json',
            data: {},
            beforeSend: this.beforeSend,
            complete: this.complete,
            success: this.success,
            error: this.error
        };
        params = $.extend(defaults, params);
        
        id = params.id || params.url;
        delete params.id;
        
        if(this.XHR[id] != null) {
			this.XHR[id].abort();
		}
        this.XHR[id] = $.ajax(params);
        console.log(params);
        
        return this.XHR[id];
   }

    this.beforeSend = function() {
    }
   
    this.complete = function(XHR, textStatus) {
        biz.XHR[this.url] = null;
    }

    this.success = function (res) {
        /* Show Error Messages */
        if (typeof res.alert!='undefined') {
            biz.alert(res.alert);
            return;
        }
        /* Show Error Messages Ends */

        if(typeof res.redirect_url!='undefined') {
            window.location.href = res.redirect_url;
        }
        return;
    }

    this.error = function (XHR, textStatus, errorThrown) {
        var err;
        if (XHR.readyState === 0 || XHR.status === 0) {
            return;
        }
        switch (textStatus) {
        case 'timeout':
            err = 'The request timed out!';
            break;
        case 'parsererror':
            err = 'Parser error!';
            break;
        case 'error':
            if (XHR.status && !/^\s*$/.test(XHR.status)) {
                err = 'Error ' + XHR.status;
            } else {
                err = 'Error';
            }
            if (XHR.responseText && !/^\s*$/.test(XHR.responseText)) {
                err = err + ': ' + XHR.responseText;
            }
            break;
        }
        if (err) {
            biz.alert([{error:[err]}]);
        }
    }

    /* messages[0] ['success'] [0] ['Actual message'] */
    this.alert = function(messages) {
        messages = messages[0];
        if (!$('#system-messages').length) {
            var dv = $('<div />').attr('id','system-messages');
            dv.appendTo($('body'));
        }
        else {
            var dv = $('#system-messages');
        }
        var error_str='';
        var success_str='';
        for(k in messages) {
            for (i in messages[k]) {
                /* Convert CActive form validation error string (non-executable json) to json.*/
                try {
                    unformatted = JSON.parse(messages[k] [i]);
                    for(attr in unformatted) {
                        error_str += '<li>' + unformatted[attr] + '</li>' + "\n";

                        $elm = $('#'+attr);
                        if($elm.length>0!=true) continue;
                        else $('form label[for="'+attr+'"]').closest('div').addClass('has-error');
                    }
                } catch (e) {
                    if (k=='success')
                        success_str += '<li>' + messages[k] [i] + '</li>' + "\n";
                    else if (k=='error')
                        error_str += '<li>' + messages[k] [i] + '</li>' + "\n";
                }
            }
        }
        if(error_str.length) {
            error_str='<ul class="list-unstyled">'+error_str+'</ul>';
            $('<div />')
            .addClass('alert alert-danger')
            .append('<button type="button" class="close" data-dismiss="alert" style="text-shadow:inherit;opacity:inherit;font-size:13px;color:#666;">Close[x]</button>')
            .append(error_str)
            .appendTo(dv);
        }
        if (success_str.length) {
            success_str='<ul class="list-unstyled">'+success_str+'</ul>';
            $('<div />')
            .addClass('alert alert-success')
            .append('<button type="button" class="close" data-dismiss="alert" style="text-shadow:inherit;opacity:inherit;font-size:13px;color:#666;">Close[x]</button>')
            .append(success_str)
            .appendTo(dv);
        }
    }
    
    if (this instanceof bizJS) {
        return this.bizJS;
    } else {
        return new bizJS();
    }
}
var biz = new bizJS();

var loadingText = '<img src="'+biz.baseUrl+'themes/theme1/assets/img/loading.gif" />&nbsp;Loading...';
var initAjaxWidgets = false;
var widgetsArr = [];

function callAjaxWidgets() {
    if (initAjaxWidgets===false) {
        initAjaxWidgets=true;
    } else {
        return;
    }
    
    console.log('total widgets: ' + $('div.ajax-widget').length);
    $('div.ajax-widget').each(function() {
        var elmId = $(this).attr('id') || 'elm_' + Math.floor((Math.random()*299)+1);
        widgetsArr.push({ 
            elm: this,
            url: $(this).data('url'), 
            prio: $(this).data('prio'), 
            h: $(this).data('height'),
            w: $(this).data('width'),
            id: elmId
        });
    });
    widgetsArr.sort(function(obj1, obj2) {return obj1.prio - obj2.prio;})
    console.log(widgetsArr);
    
    /* start loading widget loop */
    loadAjaxWidget();
}

function loadAjaxWidget() {
	if (initAjaxWidgets!=true || widgetsArr.length>0 != true) {
		return;
	}
	
    (function(widget) {
    biz.ajax({
        url: widget.url,
        beforeSend: function() {
            biz.beforeSend();
            $(widget.elm).html(loadingText);
        },
        success: function(res) {
        	var mystring=res;
        	var scriptsToRemove=[];
        	if (typeof jQuery().yiiGridView != 'undefined') {
        		scriptsToRemove.push('jquery.yiigridview.js');
        	}
        	if (typeof $.param.fragment != 'undefined') {
        		scriptsToRemove.push('jquery.ba-bbq.js');
        	}
        	$(mystring).filter('script').each(function() {
    		var srctext;
            srctext = $(this).attr('src');
            if (typeof srctext == 'undefined' || srctext.length>1 != true) return;
            for (k in scriptsToRemove) {
            switch(scriptsToRemove[k]){
            case 'jquery.yiigridview.js':
            case 'jquery.ba-bbq.js':
            	var srcexist = srctext.indexOf(scriptsToRemove[k]);
            	if(srcexist >= 0){
            	mystring = mystring.replace(srctext,'javascript:;');
            	return;
            	}
            	break;
            }
            }
        	})
        	
            $(widget.elm).html(mystring);
            try {
            documentReady();
            } catch (e) {
            }
        },
        error: function(XHR, textStatus, errorThrown) {
            $(widget.elm).html('Error 404 ' + this.url);
        },
        complete: function(XHR, textStatus) {
            biz.complete(XHR, textStatus);
        	/* remove widget reference from array */
        	widgetsArr.splice(0,1);
            /* start loading next widget in loop */
            loadAjaxWidget();
        },
        dataType: 'html'
    });
    })(widgetsArr[0]);
	
}