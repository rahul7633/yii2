<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_154913_project_task_list extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_task_list}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `creator_id` Integer Unsigned Not Null,
       `title` Varchar(255) Not Null Default "",
       `short_description` Varchar(255) Not Null Default "",
       `is_archived` Tinyint(1) Unsigned Not Null Default 0,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_154913_project_task_list cannot be reverted.\n";

        return false;
    }
}
