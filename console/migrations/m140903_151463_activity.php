<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_151463_activity extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%activity}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `item_id` Integer Unsigned Not Null Default 0,
       `item_type` Varchar(100) Not Null Default "",
       `action` Varchar(100) Not Null Default "",
       `data` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_151463_activity cannot be reverted.\n";

        return false;
    }
}
