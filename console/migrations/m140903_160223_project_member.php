<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_160223_project_member extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_member}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `user_id` Integer Unsigned Not Null,
       `role` Enum("owner","manager","contributor","member","subscriber") Not Null Default "subscriber",
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`user_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_160223_project_member cannot be reverted.\n";

        return false;
    }
}
