<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_151452_user_data extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%user_data}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `user_id` Integer Unsigned Not Null,
       `name` Varchar(255) Not Null Default "",
       `value` Varchar(255) Not Null Default "",
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`user_id`) REFERENCES {{%user}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_151452_user_data cannot be reverted.\n";

        return false;
    }
}
