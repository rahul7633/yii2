<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_174326_message_label extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%message_label}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `parent_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `title` Varchar(255) Not Null Default "",
       `is_sticky` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`parent_id`) REFERENCES {{%message_label}}(`id`)
       )');
     
     $this->execute('Create Table {{%message_label_rel_user}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `user_id` Integer Unsigned Not Null,
       `message_id` Integer Unsigned Default Null,
       `label_id` Integer Unsigned Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`user_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`message_id`) REFERENCES {{%message}}(`id`),
       FOREIGN KEY (`label_id`) REFERENCES {{%message_label}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_174326_message_label cannot be reverted.\n";

        return false;
    }
}
