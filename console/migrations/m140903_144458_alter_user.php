<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_144458_alter_user extends Migration
{
    public function up()
    {
     $this->execute('Alter Table {{%user}} Modify `id` Integer Unsigned Not Null Auto_increment,
       /*Drop `auth_key`,*/
       /*Drop `password_reset_token`,*/
       Change `password_hash` `password` Varchar(255) Not Null Default "",
       Modify `password_reset_token` Varchar(255) Not Null Default "",
       Modify `auth_key` Varchar(32) Not Null Default "",
       Modify `role` Tinyint(4) Unsigned Not Null Default 1,
       Modify `status` Tinyint(4) Unsigned Not Null Default 1,
       Modify `created_at` Datetime Not Null,
       Modify `updated_at` Datetime Not Null,
       Add Column `display_name` Varchar(255) Not Null Default "" After `username`,
       Add Column `picture` Varchar (255) Not Null Default "" After `display_name`,
       Add Column `email_verified` Tinyint(1) Unsigned Not Null Default 0 After `email`,
       Add Column `updater_id` Integer Unsigned Not Null Default 0 After `updated_at`,
       ADD UNIQUE (`username`),
       ADD UNIQUE (`email`)');
     /*$this->execute('Rename Table {{%user}} to {{%users}}');*/
    }

    public function down()
    {
     echo "m140903_144458_alter_user cannot be reverted.\n";
     
     return false;
    }
}
