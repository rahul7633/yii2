<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_153722_project extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `creator_id` Integer Unsigned Not Null,
       `title` Varchar(255) Not Null Default "",
       `short_description` Varchar(255) Not Null Default "",
       `description` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `is_archived` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `status` Enum("new", "open", "on_hold", "closed") Not Null Default "new",
       `start_date` Date Not Null,
       `closed_at` Datetime Not Null, 
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_153722_project cannot be reverted.\n";

        return false;
    }
}
