<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_161245_project_job extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_job}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `task_id` Integer Unsigned Default Null,
       `list_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `title` Varchar(255) Not Null Default "",
       `status` Enum("open","closed") Not Null Default "open",
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `closed_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`task_id`) REFERENCES {{%project_task}}(`id`),
       FOREIGN KEY (`list_id`) REFERENCES {{%project_job_list}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_161245_project_job cannot be reverted.\n";

        return false;
    }
}
