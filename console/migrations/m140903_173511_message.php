<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_173511_message extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%message}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `parent_id` Integer Unsigned Default Null,
       `group_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `subject` Varchar(255) Not Null Default "",
       `body` Text Not Null Default "",
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `sent_at` Datetime Not Null,
       `lastreply_id` Integer Unsigned Default Null,
       `lastreplied_at` Datetime Default Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`parent_id`) REFERENCES {{%message}}(`id`),
       FOREIGN KEY (`group_id`) REFERENCES {{%message}}(`id`)
       )');
     
     $this->execute('Create Table {{%message_rel_user}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `message_id` Integer Unsigned Default Null,
       `user_id` Integer Unsigned Not Null,
       `folder` Enum("inbox","sent","draft","trash","none") Not Null Default "draft",
       `is_starred` Tinyint(1) Unsigned Not Null Default 1,
       `is_important` Tinyint(1) Unsigned Not Null Default 1,
       `is_read` Tinyint(1) Unsigned Not Null Default 0,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`user_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`message_id`) REFERENCES {{%message}}(`id`),
       UNIQUE KEY (`message_id`, `user_id`)
       )');
    }

    public function down()
    {
        echo "m140903_173511_message cannot be reverted.\n";

        return false;
    }
}
