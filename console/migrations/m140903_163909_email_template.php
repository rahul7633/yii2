<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_163909_email_template extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%email_template}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `sender` Varchar(255) Not Null Default "",
       `subject` Varchar(255) Not Null Default "",
       `body` Varchar(255) Not Null Default "",
       `data` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       PRIMARY KEY (`id`)
       )');
    }

    public function down()
    {
        echo "m140903_163909_email_template cannot be reverted.\n";

        return false;
    }
}
