<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_162937_project_activity extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_activity}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `task_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `item_id` Integer Unsigned Not Null Default 0,
       `item_type` Varchar(100) Not Null Default "",
       `data` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`task_id`) REFERENCES {{%project_task}}(`id`)
       )');
     
     $this->execute('Create Table {{%project_comment}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `task_id` Integer Unsigned Default Null,
       `activity_id` Integer Unsigned Not Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `body` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Integer Unsigned Not Null Default 0,
       `updated_at` Integer Unsigned Not Null Default 0,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`task_id`) REFERENCES {{%project_task}}(`id`),
       FOREIGN KEY (`activity_id`) REFERENCES {{%project_activity}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_162937_project_activity cannot be reverted.\n";

        return false;
    }
}
