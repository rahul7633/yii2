<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_161037_project_job_list extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_job_list}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `task_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `title` Varchar(255) Not Null Default "",
       `short_description` Varchar(255) Not Null Default "",
       `is_archived` Tinyint(1) Unsigned Not Null Default 0,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`task_id`) REFERENCES {{%project_task}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_161037_project_job_list cannot be reverted.\n";

        return false;
    }
}
