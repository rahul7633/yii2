<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_162231_project_file extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_file}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `project_id` Integer Unsigned Not Null,
       `task_id` Integer Unsigned Default Null,
       `creator_id` Integer Unsigned Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `file_name` Varchar(255) Not Null Default "",
       `file_path` Varchar(255) Not Null Default "",
       `file_size` Varchar(100) Not Null Default "",
       `file_mime` Varchar(100) Not Null Default "",
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`task_id`) REFERENCES {{%project_task}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_162231_project_file cannot be reverted.\n";

        return false;
    }
}
