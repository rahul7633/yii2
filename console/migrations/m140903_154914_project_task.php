<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_154914_project_task extends Migration
{
    public function up()
    {
     $this->execute('Create Table {{%project_task}} (
       `id` Integer Unsigned Not Null Auto_increment,
       `parent_id` Integer Unsigned Default Null,
       `list_id` Integer Unsigned Not Null,
       `project_id` Integer Unsigned Not Null,
       `creator_id` Integer Unsigned Not Null,
       `owner_id` Integer Unsigned Not Null,
       `type` Enum("release","feature","bug","chore") Default "feature",
       `title` Varchar(255) Not Null Default "",
       `short_description` Varchar(255) Not Null Default "",
       `description` Text Not Null Default "",
       `is_active` Tinyint(1) Unsigned Not Null Default 1,
       `is_archived` Tinyint(1) Unsigned Not Null Default 0,
       `is_deleted` Tinyint(1) Unsigned Not Null Default 0,
       `created_at` Datetime Not Null,
       `updated_at` Datetime Not Null,
       `updater_id` Integer Unsigned Not Null Default 0,
       `status` Enum("new","started","on_hold","ready_for_testing","completed","accepted","rejected","restarted","closed") Not Null Default "new",
       `start_date` Date Not Null,
       `due_date` Date Not Null,
       `closed_at` Datetime Not Null,
       PRIMARY KEY (`id`),
       FOREIGN KEY (`creator_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`owner_id`) REFERENCES {{%user}}(`id`),
       FOREIGN KEY (`project_id`) REFERENCES {{%project}}(`id`),
       FOREIGN KEY (`parent_id`) REFERENCES {{%project_task}}(`id`),
       FOREIGN KEY (`list_id`) REFERENCES {{%project_task_list}}(`id`)
       )');
    }

    public function down()
    {
        echo "m140903_154914_project_task cannot be reverted.\n";

        return false;
    }
}
