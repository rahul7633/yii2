<?php
namespace frontend\modules\project\controllers;

use Yii;
use common\models\Project;
use common\models\project\Task;
use common\models\project\TaskList;
use common\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class TaskController extends Controller {
 
 public function behaviors() {
  $ret = parent::behaviors();
  $ret['access'] = [
   'class'=>AccessControl::className(),
   'rules'=>[
   ['allow'=>'true','roles'=>['administrator','project_owner','project_manager','project_contributor','project_member']],
   ]
  ];
  return $ret;
 }
 
 public function actionCreate($project_id) {
  $user = Yii::$app->user->getIdentity();
  $project = $this->module->findProject($project_id);
  $model = new Task();
  $model->project_id = $project->id;
  $model->creator_id = $user->id;
  $model->updater_id = $user->id;
  $model->start_date = date('Y-m-d');
  $model->due_date = date('Y-m-d', strtotime('today +3 day'));
  
  if ($model->load(Yii::$app->request->post()) && $model->save()) {
   return $this->redirect( $this->getRedirectUrl(['create', 'project_id' => $project_id]) );
  }
  
  if ( Yii::$app->getRequest()->getIsAjax() ) {
   return $this->renderAjax('create', [
     'model' => $model,
     'project' => $project
   ]);
  }
  
  return $this->render('create', [
    'model' => $model,
    'project' => $project
    ]);
 }
 
 public function actionView($id) {
  return $this->render('view', [
    'model' => $this->module->findTask($id),
    ]);
 }
 
}