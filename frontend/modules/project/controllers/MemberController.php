<?php
namespace frontend\modules\project\controllers;

use Yii;
use common\models\Project;
use common\models\project\Member;
use common\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MemberController extends Controller {
 
 public function behaviors() {
  $ret = parent::behaviors();
  $ret['access'] = [
   'class'=>AccessControl::className(),
   'rules'=>[
   ['allow'=>'true','roles'=>['administrator','project_owner','project_manager']],
   ]
  ];
  return $ret;
 }
 
 public function actionCreate($project_id) {
  $user = Yii::$app->user->getIdentity();
  $project = $this->module->findProject($project_id);
  $model = new Member();
  $model->project_id = $project->id;
  $model->creator_id = $user->id;
  $model->updater_id = $user->id;
  
  if ($model->load(Yii::$app->request->post()) && $model->save()) {
   return $this->redirect( $this->getRedirectUrl(['create', 'project_id' => $project_id]) );
  }
  
  if ( Yii::$app->getRequest()->getIsAjax() ) {
   return $this->renderAjax('create', [
     'model' => $model,
     'project' => $project
     ]);
  }
  
  return $this->render('create', [
    'model' => $model,
    'project' => $project
    ]);
 }
 
}