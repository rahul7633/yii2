<?php
namespace frontend\modules\project\controllers;

use Yii;
use common\models\Project;
use common\models\project\Task;
use common\models\project\Job;
use common\models\project\JobList;
use common\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class JobListController extends Controller {
 
 public function behaviors() {
  $ret = parent::behaviors();
  $ret['access'] = [
   'class'=>AccessControl::className(),
   'rules'=>[
   ['allow'=>'true','roles'=>['administrator','project_owner','project_manager','project_contributor','project_member']],
   ]
  ];
  return $ret;
 }
 
 public function actionCreate($project_id, $task_id=null) {
  $user = Yii::$app->user->getIdentity();
  $project = $this->module->findProject($project_id);
  $model = new JobList;
  $model->project_id = $project->id;
  $model->creator_id = $user->id;
  $model->updater_id = $user->id;
  if ( $task_id != null ) {
   $task = $this->module->findTask($task_id);
   $model->task_id = $task->id;
  }
  
  if ($model->load(Yii::$app->request->post()) && $model->save()) {
   return $this->redirect( $this->getRedirectUrl(['create', 'project_id' => $project_id, 'task_id' => $task_id]) );
  }
  
  if ( Yii::$app->getRequest()->getIsAjax() ) {
   return $this->renderAjax('create', [
     'model' => $model,
     'project' => $project
     ]);
  }
  
  return $this->render('create', [
    'model' => $model,
    'project' => $project
    ]);
 }
 
}