<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use frontend\assets\AppAsset;
use common\models\Project;
use common\models\project\Task;
use common\models\project\Job;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Task */
/* @var $form yii\widgets\ActiveForm */

/* new scripts */
$asset = AppAsset::register($this);
?>

<div class="job-form">

    <?php echo Html::errorSummary($model) ?>
    <?php $form = ActiveForm::begin([
     'id' => 'job-create'
    ]); ?>
    
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'list_id', ['inputOptions'=>['class'=>'form-control']])->dropDownList($jobListOptions, ['prompt' => '']) ?>
    
    <?= $form->field($model, 'status', ['inputOptions'=>['class'=>'form-control']])->dropDownList(Job::$statusOptions, ['prompt' => '']) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
