<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\TaskList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taskList-form">

    <?php echo Html::errorSummary($model) ?>
    <?php $form = ActiveForm::begin([
     'id' => 'taskList-create'
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short_description')->textarea(['maxlength' => 255, 'rows'=>3]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>