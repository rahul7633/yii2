<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Message */
/* @var $form yii\widgets\ActiveForm */

/* new scripts */
$asset = AppAsset::register($this);

$this->title = 'Add New Message';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['/project/default/index']];
$this->params['breadcrumbs'][] = ['label' => $project->title, 'url' => ['/project/default/view', 'id'=>$project->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="project_message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'project' => $project
    ]) ?>
    

</div>