<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Message */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="project_message-form">

    <?php echo Html::errorSummary($model) ?>
    <?php $form = ActiveForm::begin([
     'id' => 'message-create'
    ]); ?>

    <?= $form->field($model, 'body')->textarea(['maxlength' => 255, 'rows'=>6]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>