<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="task">
    <div class="task-title">
        <?php echo $this->render('/default/_label', array('data'=>$model)); ?>
        <?php echo '&nbsp;<h2>' . Html::encode($model->title) . '</h2>'; ?>
    </div>
    <p class=""><?php echo Html::encode($model->short_description); ?>
    <small class="task-info">
        <b>ID</b>: <?php echo $model->id; ?>
        <b>Creator</b>: <?php echo $model->user->display_name; ?>
        <b>Start Date</b>: <?php echo $model->formatDate($model->start_date); ?>
    </small>
    </p>
    
    <!-- Split button -->
    <div class="btn-group " style="">
      <button type="button" class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">
        New&nbsp;<span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="<?php echo Url::to(array('/project/task/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Task</a></li>
        <li><a href="<?php echo Url::to(array('/project/task-list/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Task List</a></li>
        <li><a href="<?php echo Url::to(array('/project/job/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Job</a></li>
        <li><a href="<?php echo Url::to(array('/project/job-list/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Job List</a></li>
        <li><a href="<?php echo Url::to(array('/project/message/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Activity</a></li>
        <li><a href="<?php echo Url::to(array('/project/member/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Member</a></li>
        <li><a href="<?php echo Url::to(array('/project/file/create', 'project_id'=>$model->id, 'redirect_url'=>'referer'))?>" class="modalButton">Attachment</a></li>
      </ul>
    </div>
    
    <!-- Single button -->
    <div class="btn-group " style="">
      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
        More&nbsp;<span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="<?php echo Url::to(array('/project/default/update', 'id'=>$model->id, 'redirect_url'=>'referer')) ?>" class="">Edit Details</a></li>
        <li><a href="<?php echo Url::to(array('/project/member/index', 'project_id'=>$model->id)) ?>" class="">Members</a></li>
        <li><a href="<?php echo Url::to(array('/project/search', 'project_id'=>$model->id)) ?>" class="">Project Search</a></li>
      </ul>
    </div>
    
    <a href="<?php echo Url::to(array('/project/default/archive', 'id'=>$model->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-warning ajax">Archive</a>
    <a href="<?php echo Url::to(array('/project/default/delete', 'id'=>$model->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-danger ajax" data-confirm="Are you sure you want to delete this item?" data-method="post">Delete</a>
    
</div><!--/task-->