<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\project\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="panel panel-info">
    <div class="panel-heading">Filter your results</div>
    <div class="panel-body">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    </div>
    
    <div class="panel panel-info">
    <div class="panel-heading">Search Results</div>
    <div class="panel-body project-item">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '/default/_item',
        'layout' => "{items}\n{pager}"
        ]); ?>
    </div>
    </div>

</div>
