<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use frontend\assets\AppAsset;
use frontend\modules\project\models\Project;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */

/* new scripts */
$asset = AppAsset::register($this);
?>

<div class="project-form">

    <?php $form = ActiveForm::begin([
     'id' => 'project-create'
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short_description')->textarea(['maxlength' => 255, 'rows'=>3]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status', ['inputOptions'=>['class'=>'form-control']])->dropDownList(Project::$statusOptions, ['prompt' => '']) ?>

    <?= $form->field($model, 'start_date')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $asset->addDatePicker($this);
$asset->addMultiSelect($this);
$this->registerJs('jQuery("#project-start_date").datepicker({format:"yyyy-mm-dd"});
$(".multiselect").multiselect({maxHeight:400,enableFiltering:true,enableCaseInsensitiveFiltering:true});', View::POS_READY);
?>