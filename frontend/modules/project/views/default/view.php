<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\project\widgets\JobList;
use frontend\modules\project\widgets\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-info">
    <div class="panel-heading">Basic Details</div>
    <div class="panel-body project-item">
    <?php echo $this->render( '/default/_item', ['model'=>$model] ); ?>
    </div>
    </div>
    
    <!-- <p>
        <?php /*Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])*/ ?>
    </p> -->
    
    <div class="panel panel-info">
        <div class="panel-heading">
        Full Description
        </div>
        <div class="panel-body project-description">
        <?php echo $model->description; ?>
      </div>
    </div>
    <div class="clearfix odd"></div>
    
    <?php echo JobList::widget([
     'dataProvider' => $model->getJobLists()
    ]) ?>
    
    <?php echo Message::widget([
     'dataProvider' => $model->getMessages()
    ]) ?>
    
</div>
