<span class="label label-default">Project</span>
    <?php switch($data->status):
    case 'new':
        echo '<span class="label label-info">New</span>';
        break;
    case 'open':
        echo '<span class="label label-info">Open</span>';
        break;
    case 'closed':
        echo '<span class="label label-warning">Closed</span>';
        break;
    case 'delivered':
        echo '<span class="label label-success">Delivered</span>';
        break;
    endswitch; ?>