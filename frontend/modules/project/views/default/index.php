<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\project\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
             'attribute'=>'user',
             'value'=>'user.display_name',
            ],
            'short_description',
            // 'description:ntext',
            'start_date',
            [
             'attribute'=>'status',
             'value'=>function($data){return ucwords($data->status);},
             'filter'=>['new'=>'New','open'=>'Open','on_hold'=>'On Hold','closed'=>'Closed']
            ],
            // 'is_active',
            // 'is_archived',
            // 'created_at',
            // 'updated_at',
            // 'updater_id',
            // 'status',
            // 'start_date',
            // 'closed_at',

            [
             'class' => 'common\grid\ActionColumn',
             'buttons' => [
              'add_task'=>function($url, $model, $key){
               $url = Url::to(array('/project/task/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Task</a>';
              },
              'add_taskList'=>function($url, $model, $key){
               $url = Url::to(array('/project/task-list/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Task List</a>';
              },
              'add_job'=>function($url, $model, $key){
               $url = Url::to(array('/project/job/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Job</a>';
              },
              'add_jobList'=>function($url, $model, $key){
               $url = Url::to(array('/project/job-list/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Job List</a>';
              },
              'add_member'=>function($url, $model, $key){
               $url = Url::to(array('/project/member/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Member</a>';
              },
              'add_file'=>function($url, $model, $key){
               $url = Url::to(array('/project/file/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Attachment</a>';
              },
              'add_message'=>function($url, $model, $key){
               $url = Url::to(array('/project/message/create', 'project_id'=>$model->id));
               return '<a href="'.$url.'">Activity</a>';
              },
              'archive'=>function($url, $model, $key){
               return '<a href="'.$url.'" class="btn btn-warning btn-xs">Archive</a>';
              },
            ],
            /*'urlCreator'=>function($action, $model, $key, $index){
               $params = ['id' => $model->id];
               $params[0] = $action;
               
               return Url::toRoute($params);
              },*/
            'template'=>'{view}
               <!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
    New <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li>{add_task}</li>
    <li>{add_taskList}</li>
    <li>{add_job}</li>
    <li>{add_jobList}</li>
    <li>{add_message}</li>
    <li>{add_member}</li>
    <li>{add_file}</li>
    <!--<li class="divider"></li>
    <li><a href="#">Separated link</a></li>-->
  </ul>
</div>
{archive} {update} {delete}',
            ],
        ],
    ]); ?>

</div>
