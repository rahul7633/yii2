<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use yii\web\View;
use frontend\modules\project\models\Search;
use frontend\assets\AppAsset;
use kartik\widgets\Typeahead;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\Search */
/* @var $form yii\widgets\ActiveForm */

/* new scripts */
$asset = AppAsset::register($this);
?>

<div class="project-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>

    <?php echo $form->field($model, 'user', ['labelOptions'=>['style'=>'text-align:left;']])->widget(Typeahead::classname(), [
    'options' => ['placeholder' => 'Filter project owner ...'],
    'dataset' => [
        [
            'remote' => [
             'url'=>urldecode( Url::to( ['/autosuggest/members', 'q'=>'%QUERY'] ) ),
             'ajax' => ['success'=>'function(data){console.log(data)}']
            ],
            'limit' => 10,
        ]
    ],
    'pluginOptions' => ['highlight'=>true],
]); ?>

    <?php echo $form->field($model, 'title', ['labelOptions'=>['style'=>'text-align:left;']])->textInput(['maxlength' => 255]) ?>

    <?php echo $form->field($model, 'status', ['labelOptions'=>['style'=>'text-align:left;'],'inputOptions'=>['class'=>'form-control']])->widget(Select2::classname(), [
    'data' => Search::$statusOptions,
    'options' => ['placeholder' => 'Filter project status ...', 'multiple'=>true],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>
    
    <?php echo $form->field($model, 'is_archived', ['labelOptions'=>['style'=>'text-align:left;']])->checkbox() ?>
    <?php /*echo $form->field($model, 'is_deleted')->checkbox([], false)*/ ?>

    <?php echo $form->field($model, 'start_date', ['labelOptions'=>['style'=>'text-align:left;']]) ?>

    <?php echo $form->field($model, 'closed_at', ['labelOptions'=>['style'=>'text-align:left;']]) ?>

    <div class="form-group">
    <div class=col-sm-offset-2 col-sm-10>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$asset->addDateRangePicker($this);
$this->registerJs('jQuery("#search-start_date,#search-closed_at").daterangepicker({});', View::POS_READY);
?>
