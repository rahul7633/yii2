<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="task">
    <div class="task-title">
        <?php echo $this->render('/task/_label', array('data'=>$data)); ?>
        <?php echo '&nbsp;<h2>' . Html::encode($data->title) . '</h2>'; ?>
    </div>
    <p class=""><?php echo Html::encode($data->short_description); ?>
    <small class="task-info">
        <b>ID</b>: <?php echo $data->id; ?>
        <b>List</b>: <?php echo $data->taskList->title; ?>
        <b>Requester</b>: <?php echo $data->user->display_name; ?>
        <b>Owner</b>: <?php echo $data->owner->display_name; ?>
        <b>Due Date</b>: <?php echo $data->formatDate($data->due_date); ?>
    </small>
    </p>
    
    <!-- Split button -->
    <div class="btn-group " style="">
      <button type="button" class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">
        New&nbsp;<span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="<?php echo Url::to(array('/project/job/create', 'project_id'=>$data->project_id, 'task_id'=>$data->id, 'redirect_url'=>'referer'))?>" class="modalButton">Job</a></li>
        <li><a href="<?php echo Url::to(array('/project/job-list/create', 'project_id'=>$data->project_id, 'task_id'=>$data->id, 'redirect_url'=>'referer'))?>" class="modalButton">Job List</a></li>
        <li><a href="<?php echo Url::to(array('/project/message/create', 'project_id'=>$data->project_id, 'task_id'=>$data->id, 'redirect_url'=>'referer'))?>" class="modalButton">Activity</a></li>
        <li><a href="<?php echo Url::to(array('/project/file/create', 'project_id'=>$data->project_id, 'task_id'=>$data->id, 'redirect_url'=>'referer'))?>" class="modalButton">Attachment</a></li>
      </ul>
    </div>
    
    <!-- Single button -->
    <div class="btn-group " style="">
      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
        More&nbsp;<span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="<?php echo Url::to(array('/project/task/update', 'id'=>$data->id, 'redirect_url'=>'referer')) ?>" class="">Edit Details</a></li>
      </ul>
    </div>
    
    <?php echo $this->render('/task/_status', array('data'=>$data)) ;?>
    
    <?php /*<a href="<?php echo Url::to(array('/project/task/archive', 'id'=>$data->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-warning ajax">Archive</a>
    <a href="<?php echo Url::to(array('/project/task/delete', 'id'=>$data->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-danger ajax" data-confirm="Are you sure you want to delete this item?" data-method="post">Delete</a>*/ ?>
    
</div><!--/task-->