    <span class="label label-default"><?php echo ucwords(strtolower($data->type)) ?></span>
    <?php switch ($data->status): 
    case 'new':
        echo '<span class="label label-info">New</span>';
        break;
    case 'started':
        echo '<span class="label label-primary">Started</span>';
        break;
    case 'on_hold':
        echo '<span class="label label-warning">On Hold</span>';
        break;
    case 'ready_for_testing':
        echo '<span class="label label-primary">Ready for Testing</span>';
        break;
    case 'completed':
        echo '<span class="label label-primary">Finished</span>';
        break;
    case 'accepted':
        echo '<span class="label label-success">Accepted</span>';
        break;
    case 'rejected':
        echo '<span class="label label-danger">Rejected</span>';
        break;
    case 'restarted':
        echo '<span class="label label-primary">Restarted</span>';
        break;
    case 'closed':
        echo '<span class="label label-warning">Closed</span>';
        break;
    case 'delivered':
        echo '<span class="label label-success">Delivered</span>';
        break;
    endswitch;?>
    <?php /*switch ($data->priority): 
    case 'urgent':
        echo '<span class="label label-warning">Urgent</span>';
        break;
    case 'high':
        echo '<span class="label label-warning">High</span>';
        break;
    endswitch;*/ 
    ?>