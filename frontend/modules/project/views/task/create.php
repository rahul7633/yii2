<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Task */

$this->title = 'Add New Task';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['/project/default/index']];
$this->params['breadcrumbs'][] = ['label' => $project->title, 'url' => ['/project/default/view', 'id'=>$project->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'project' => $project,
    ]) ?>

</div>
