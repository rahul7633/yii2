<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use frontend\assets\AppAsset;
use common\models\Project;
use common\models\project\Task;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Task */
/* @var $form yii\widgets\ActiveForm */

/* new scripts */
$asset = AppAsset::register($this);
?>

<div class="task-form">

    <?php echo Html::errorSummary($model) ?>
    <?php $form = ActiveForm::begin([
     'id' => 'task-create',
    ]); ?>

    <?= $form->field($model, 'type', ['inputOptions'=>['class'=>'form-control']])->dropDownList(Task::$typeOptions, ['prompt' => '']) ?>
    
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short_description')->textarea(['maxlength' => 255, 'rows'=>3]) ?>

    <?= $form->field($model, 'list_id', ['inputOptions'=>['class'=>'form-control']])->dropDownList($project->taskListOptions, ['prompt' => '']) ?>
    
    <?= $form->field($model, 'owner_id', ['inputOptions'=>['class'=>'form-control']])->dropDownList($project->memberOptions, ['prompt' => '']) ?>
    
    <?= $form->field($model, 'status', ['inputOptions'=>['class'=>'form-control']])->dropDownList(Task::$statusOptions, ['prompt' => '']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'start_date')->textInput(['maxlength' => 10]) ?>
    
    <?= $form->field($model, 'due_date')->textInput(['maxlength' => 10]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $asset->addDatePicker($this);
$asset->addMultiSelect($this);
$this->registerJs('jQuery("#task-start_date,#task-due_date").datepicker({format:"yyyy-mm-dd"});
$(".multiselect").multiselect({maxHeight:400,enableFiltering:true,enableCaseInsensitiveFiltering:true});', View::POS_READY);
?>