<?php 
use yii\helpers\Html;
use yii\helpers\Url;

switch($data->status): 
    case 'new':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'started', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-default ajax">Start</a>';
        break;
    case 'started':
    case 'ready_for_testing':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'completed', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-primary ajax">Finish</a>';
        break;
    case 'completed':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'accepted', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-success ajax">Accept</a>' . "\n";
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'rejected', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-danger ajax">Reject</a>';
        break;
    case 'rejected':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'restarted', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-primary ajax">Restart</a>';
        break;
    case 'restarted':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'completed', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-primary ajax">Finish</a>';
        break;
    case 'accepted':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'closed', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-warning ajax">Close</a>';
        break;
    case 'closed':
        echo '<a href="'.Url::to(array('/project/task/upd_status', 'status'=>'delivered', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')).'" class="btn btn-xs btn-success ajax">Deliver</a>';
        break;
    endswitch; ?>

<?php if($data->is_archived!='1'): ?>
<a href="<?php echo Url::to(array('/project/task/archive', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-default ajax">Archive</a>    
<?php endif; ?>

<?php if($data->is_deleted!='1'): ?>
<a href="<?php echo Url::to(array('/project/task/delete', 'project_id'=>$data->project_id, 'id'=>$data->id, 'redirect_url'=>'referer')); ?>" class="btn btn-xs btn-danger ajax" data-confirm="Are you sure you want to delete this item?" data-method="post">Delete</a>    
<?php endif; ?>
