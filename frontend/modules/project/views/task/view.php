<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\project\widgets\JobList;
use frontend\modules\project\widgets\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Task */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->project->title, 'url' => ['/project/default/view', 'id' => $model->project->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-info">
    <div class="panel-heading">Basic Details</div>
    <div class="panel-body task-item">
    <?php echo $this->render( '/task/_item', ['data'=>$model] ); ?>
    </div>
    </div>
    
    <div class="panel panel-info">
        <div class="panel-heading">
        Full Description
        </div>
        <div class="panel-body task-description">
        <?php echo $model->description; ?>
      </div>
    </div>
    <div class="clearfix odd"></div>
    
    <?php echo JobList::widget([
     'dataProvider' => $model->getJobLists()
    ]) ?>
    
    <?php echo Message::widget([
     'dataProvider' => $model->getMessages()
    ]) ?>
    
</div>
