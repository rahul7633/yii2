<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\jui\JuiAsset;
use yii\web\JsExpression;
use common\models\project\Member;

/* @var $this yii\web\View */
/* @var $project common\models\Project */
/* @var $model common\models\project\Member */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="project_member-form">

    <?php echo Html::errorSummary($model) ?>
    <?php $form = ActiveForm::begin([
     'id' => 'member-create'
    ]); ?>

    <?= $form->field($model, 'user')->textInput(['maxlength' => 255])->widget(AutoComplete::className(), [
     'clientOptions' => [
      'source' => Url::to(array('/autosuggest/members')),
      'delay' => 1000,
      'autoFill' => true,
      'minLength' => '2',
      'select' => new JsExpression("function( event, ui ) {
       $('#member-user_id').val(ui.item.id);
      }")],
     'options'=>['class'=>'form-control']
    ]) ?>
    
    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
    
    <?= $form->field($model, 'role', ['inputOptions'=>['class'=>'form-control']])->dropDownList(Member::$roleOptions, ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>