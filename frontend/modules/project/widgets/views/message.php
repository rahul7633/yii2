<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm; ?>

<div class="panel panel-info">
 <div class="panel-heading">
 Messages
 </div>
<div class="panel-body">
<?php foreach ( $dataProvider->all() as $data ) :
echo '<span class="username">'.$data->user->display_name.'</span>' . '<br />';
echo '<span class="date text-muted"><small>'.$data->formatDateTime($data->created_at).'</small></span>';
echo '<p class="qd-line">' . $data->body . '</p>';
endforeach; ?>
</div>
</div>