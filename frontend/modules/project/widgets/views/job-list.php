<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

foreach($dataProvider->all() as $list): ?>
<div class="panel panel-info">
        <div class="panel-heading">
        <?php echo $list->title; ?>
        </div>
        <div class="panel-body">
        <div class="row">
        <?php $form = ActiveForm::begin() ?>
        <?php foreach($list->jobs as $data): ?>
        <div class="col-lg-8" style="margin-bottom:10px;">
        <div class="confirm" style="position:relative;">
            <label style="">
            <?php echo $data->title ;?>
            <small class="text-muted qd-line">
            <?php echo $data->user->display_name ?> at <?php echo $data->formatDateTime($data->created_at); ?>
            </small>
            </label>
        </div></div>
        <?php endforeach; ?>
        <?php ActiveForm::end(); ?>
        </div><!--/row-->
        <div class="clearfix odd"></div>
 
        <div class="row">
        <?php $form = ActiveForm::begin([
         'method' => 'post',
         'action' => ['/project/job/create', 'project_id'=>$list->project_id, 'task_id'=>$list->task_id],
        ]);
        echo Html::hiddenInput('Job[list_id]', $list->id);
        echo Html::hiddenInput('Job[status]', 'open');
        echo Html::hiddenInput('redirect_url', 'referer');
        ?>
        <div class="form-group col-lg-8">
            <?php echo Html::label('Job title','job_title',array('class'=>'sr-only')); ?>
            <?php echo Html::textInput('Job[title]','',array('class'=>'form-control input-sm','id'=>'job_title','placeholder'=>'Write here to add new job.','autocomplete'=>'off')); ?>
        </div>
        <?php echo Html::buttonInput('Submit', array('class'=>'btn btn-primary','type'=>'submit','name'=>'Job[btn_submit]','style'=>'display:none;')); ?>
        
        <?php ActiveForm::end(); ?>
        </div><!--/row-->
        <div class="clearfix odd"></div>
        
        </div><!--/box-contents-->
</div>
<?php endforeach; ?>