<?php
namespace frontend\modules\project\widgets;

use yii\base\Widget;
use common\models\Project;
use common\models\project\Task;
use common\models\project\JobList As JobListModel;
use common\models\project\Job;

class JobList extends Widget {
 public $dataProvider;
 
 public function init() {
  
 }
 
 public function run() {
  return $this->render('job-list', [
   'dataProvider' => $this->dataProvider
  ]);
 }
}