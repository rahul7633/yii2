<?php
namespace frontend\modules\project\widgets;

use yii\base\Widget;
use common\models\Project;
use common\models\project\Task;
use common\models\project\Message As MessageModel;

class Message extends Widget {
 public $dataProvider;
 
 public function init() {
  
 }
 
 public function run() {
  return $this->render('message', [
   'dataProvider' => $this->dataProvider
  ]);
 }
}