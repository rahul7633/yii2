<?php

namespace frontend\modules\project;

use Yii;
use yii\web\NotFoundHttpException;
use common\models\Project;
use common\models\project\Task;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\project\controllers';
    private $_project = null,
     $_task = null;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    
    public function findProject($project_id=null) 
    {
     if ( $this->_project !== null ) {
      return $this->_project;
     }
     if ( $project_id == null ) {
      $project_id = Yii::$app->request->get('project_id');
     }
     $project_id = intval( $project_id );
     $this->_project = Project::find()->where(['project.id'=>$project_id])->joinWith('user')->one();
     if ($this->_project instanceof Project !== true) {
      throw new NotFoundHttpException('The requested page does not exist.');
     }
     return $this->_project;
    }
    
    public function findTask($task_id=null)
    {
     if ( $this->_task !== null ) {
      return $this->_task;
     }
     if ( $task_id == null ) {
      $task_id = Yii::$app->request->get('task_id');
     }
     $task_id = intval( $task_id );
     $this->_task = Task::find()->from(['task'=>Task::tableName()])->where(['task.id'=>$task_id])->joinWith('user')->one();
     if ($this->_task instanceof Task !== true) {
      throw new NotFoundHttpException('The requested page does not exist.');
     }
     return $this->_task;
    }
}
