<?php

namespace frontend\modules\project\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Project;

/**
 * Search represents the model behind the search form about `common\models\Project`.
 */
class Search extends Project
{
    public $user;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_archived', 'created_at', 'updated_at', 'creator_id', 'updater_id'], 'integer'],
            [['title', 'short_description', 'description', 'status'], 'safe'],
            [['start_date', 'closed_at', 'user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query->joinWith(['user']);
        $dataProvider->sort->attributes['user'] = [
        'asc'=>['user.display_name'=>SORT_ASC],
        'desc'=>['user.display_name'=>SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $alias = 'project';
        $query->andFilterWhere([
            $alias.'.id' => $this->id,
            $alias.'.is_active' => $this->is_active,
            $alias.'.is_archived' => $this->is_archived,
            $alias.'.created_at' => $this->created_at,
            $alias.'.updated_at' => $this->updated_at,
            $alias.'.creator_id' => $this->creator_id,
            $alias.'.updater_id' => $this->updater_id,
            $alias.'.start_date' => $this->start_date,
            $alias.'.closed_at' => $this->closed_at,
        ]);

        $query->andFilterWhere(['like', $alias.'.title', $this->title])
            ->andFilterWhere(['like', $alias.'.short_description', $this->short_description])
            ->andFilterWhere(['like', $alias.'.description', $this->description])
            ->andFilterWhere(['in', $alias.'.status', $this->status])
            ->andFilterWhere(['like', 'user.display_name', $this->user]);

        return $dataProvider;
    }
    
}
