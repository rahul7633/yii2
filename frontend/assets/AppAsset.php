<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';*/
    public $css = [
        '../../css/site.css',
    ];
    public $js = [
        '../../scripts/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $sourcePath = '@common/web/assets';
    
    public static function register($view) {
     return parent::register($view);
    }
    
    public function addDatePicker($view) {
     $this->js[] = 'datepicker/datepicker.js';
     $this->css[] = 'datepicker/datepicker.css';     
    }
    
    public function addMultiSelect($view) {
     $this->js[] = 'multiselect/js/bootstrap-multiselect.js';
     $this->css[] = 'multiselect/css/bootstrap-multiselect.css';
    }
    
    public function addDateRangePicker($view) {
     $this->js[] = 'daterangepicker/moment.min.js';
     $this->js[] = 'daterangepicker/daterangepicker.js';
     $this->css[] = 'daterangepicker/daterangepicker-bs3.css';
    }
}
