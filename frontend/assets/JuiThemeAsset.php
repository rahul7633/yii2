<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class JuiThemeAsset extends AssetBundle {
 public $sourcePath = '@bower/jquery-ui/themes/flick/';
 public $css = [
 'jquery-ui.min.css',
 ];
}