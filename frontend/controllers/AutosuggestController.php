<?php
namespace frontend\controllers;

use Yii;
use yii\web\NotAcceptableHttpException;
use common\web\Controller;
use common\models\User;

class AutosuggestController extends Controller {
 public function actionMembers() {
  $request = Yii::$app->getRequest();
  $term = $request->get('q');
  if ( empty($term) ) {
   $term = $request->get('term');
  }
  $term = trim($term);
  $items = User::find()
   ->select('user.id,user.display_name')
   ->where(['like','display_name',$term])
   ->limit(10)
   ->all();
  $return = [];
  foreach ( $items as $item ) {
   $return [] = ['value'=>$item->display_name, 'id'=>$item->id];
  }
  echo json_encode($return);
  exit;
 }
}
