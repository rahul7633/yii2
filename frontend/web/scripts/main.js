// listen click, open modal and .load content
$('.modalButton').on( 'click', function (){
    $('#modal').modal('show')
        .find('#modalContent')
        .load($(this).attr('href'), function(){
        	var $modalTitle = $('#modalContent').find('h1');
        	$('#modalTitle').html('<h4>'+$modalTitle.text()+'</h4>');
        	$modalTitle.remove();
        });
    return false;
} );